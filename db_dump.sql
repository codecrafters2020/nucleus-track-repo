--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 9.5.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_storage_attachments; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.active_storage_attachments (
    id bigint NOT NULL,
    name character varying NOT NULL,
    record_type character varying NOT NULL,
    record_id bigint NOT NULL,
    blob_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.active_storage_attachments OWNER TO junaid;

--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.active_storage_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_storage_attachments_id_seq OWNER TO junaid;

--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.active_storage_attachments_id_seq OWNED BY public.active_storage_attachments.id;


--
-- Name: active_storage_blobs; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.active_storage_blobs (
    id bigint NOT NULL,
    key character varying NOT NULL,
    filename character varying NOT NULL,
    content_type character varying,
    metadata text,
    byte_size bigint NOT NULL,
    checksum character varying NOT NULL,
    created_at timestamp without time zone NOT NULL
);


ALTER TABLE public.active_storage_blobs OWNER TO junaid;

--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.active_storage_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.active_storage_blobs_id_seq OWNER TO junaid;

--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.active_storage_blobs_id_seq OWNED BY public.active_storage_blobs.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO junaid;

--
-- Name: bids; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.bids (
    id bigint NOT NULL,
    company_id integer,
    amount double precision,
    status integer,
    detention integer,
    shipment_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.bids OWNER TO junaid;

--
-- Name: bids_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.bids_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bids_id_seq OWNER TO junaid;

--
-- Name: bids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.bids_id_seq OWNED BY public.bids.id;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.companies (
    id bigint NOT NULL,
    company_type character varying,
    category character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_contractual boolean DEFAULT false,
    verified boolean DEFAULT false
);


ALTER TABLE public.companies OWNER TO junaid;

--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO junaid;

--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.companies_id_seq OWNED BY public.companies.id;


--
-- Name: company_informations; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.company_informations (
    id bigint NOT NULL,
    name character varying,
    address character varying,
    landline_no character varying,
    website_address character varying,
    license_number character varying,
    license_expiry_date character varying,
    secondary_contact_name character varying,
    secondary_mobile_no character varying,
    news_update boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    company_id character varying,
    is_complete boolean DEFAULT false,
    avatar json,
    documents character varying[] DEFAULT '{}'::character varying[]
);


ALTER TABLE public.company_informations OWNER TO junaid;

--
-- Name: company_informations_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.company_informations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_informations_id_seq OWNER TO junaid;

--
-- Name: company_informations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.company_informations_id_seq OWNED BY public.company_informations.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.countries (
    id bigint NOT NULL,
    name character varying,
    dialing_code character varying,
    process character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    commision double precision,
    tax double precision,
    currency character varying,
    penalty_free_hours double precision,
    status boolean
);


ALTER TABLE public.countries OWNER TO junaid;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.countries_id_seq OWNER TO junaid;

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;


--
-- Name: driver_informations; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.driver_informations (
    id bigint NOT NULL,
    national_id character varying,
    expiry_date date,
    license character varying,
    license_expiry date,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.driver_informations OWNER TO junaid;

--
-- Name: driver_informations_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.driver_informations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.driver_informations_id_seq OWNER TO junaid;

--
-- Name: driver_informations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.driver_informations_id_seq OWNED BY public.driver_informations.id;


--
-- Name: individual_informations; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.individual_informations (
    id bigint NOT NULL,
    secondary_mobile_no character varying,
    landline_no character varying,
    national_id character varying,
    expiry_date character varying,
    news_update boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    company_id integer,
    is_complete boolean DEFAULT false,
    avatar json,
    documents character varying[] DEFAULT '{}'::character varying[]
);


ALTER TABLE public.individual_informations OWNER TO junaid;

--
-- Name: individual_informations_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.individual_informations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.individual_informations_id_seq OWNER TO junaid;

--
-- Name: individual_informations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.individual_informations_id_seq OWNED BY public.individual_informations.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.locations (
    id bigint NOT NULL,
    pickup_location character varying,
    pickup_lat character varying,
    pickup_lng character varying,
    pickup_city character varying,
    drop_location character varying,
    drop_lat character varying,
    drop_lng character varying,
    drop_city character varying,
    rate numeric,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    country_id integer
);


ALTER TABLE public.locations OWNER TO junaid;

--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.locations_id_seq OWNER TO junaid;

--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.locations_id_seq OWNED BY public.locations.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO junaid;

--
-- Name: shipment_vehicles; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.shipment_vehicles (
    id bigint NOT NULL,
    shipment_id integer,
    vehicle_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status integer
);


ALTER TABLE public.shipment_vehicles OWNER TO junaid;

--
-- Name: shipment_vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.shipment_vehicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shipment_vehicles_id_seq OWNER TO junaid;

--
-- Name: shipment_vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.shipment_vehicles_id_seq OWNED BY public.shipment_vehicles.id;


--
-- Name: shipments; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.shipments (
    id bigint NOT NULL,
    pickup_location character varying,
    pickup_lat character varying,
    pickup_lng character varying,
    drop_location character varying,
    drop_lat character varying,
    drop_lng character varying,
    pickup_date date,
    pickup_time timestamp without time zone,
    loading_time double precision,
    expected_drop_off date,
    unloading_time double precision,
    no_of_vehicles integer,
    cargo_description text,
    cargo_packing_type character varying,
    company_id integer,
    vehicle_id integer,
    approved boolean DEFAULT false,
    shipping_request_type_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    state character varying,
    amount numeric,
    location_id integer,
    pickup_city character varying,
    drop_city character varying,
    country_id integer,
    process character varying,
    vehicle_type_id integer,
    fleet_id integer,
    payment_option character varying,
    cancel_by character varying,
    cancel_penalty numeric
);


ALTER TABLE public.shipments OWNER TO junaid;

--
-- Name: shipments_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.shipments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shipments_id_seq OWNER TO junaid;

--
-- Name: shipments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.shipments_id_seq OWNED BY public.shipments.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    first_name character varying,
    last_name character varying,
    mobile character varying,
    company_id integer,
    role integer,
    mobile_verified character varying,
    verification_code character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    terms_accepted boolean DEFAULT false,
    country_id integer,
    device_id text,
    os character varying,
    deleted_at timestamp without time zone
);


ALTER TABLE public.users OWNER TO junaid;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO junaid;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: vehicle_types; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.vehicle_types (
    id bigint NOT NULL,
    code integer,
    name text,
    country_id integer,
    fleet_penalty_amount numeric,
    cargo_penalty_amount numeric,
    base_rate numeric,
    free_kms double precision,
    rate_per_km numeric,
    waiting_charges numeric,
    load_unload_free_hours double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.vehicle_types OWNER TO junaid;

--
-- Name: vehicle_types_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.vehicle_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehicle_types_id_seq OWNER TO junaid;

--
-- Name: vehicle_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.vehicle_types_id_seq OWNED BY public.vehicle_types.id;


--
-- Name: vehicles; Type: TABLE; Schema: public; Owner: junaid
--

CREATE TABLE public.vehicles (
    id bigint NOT NULL,
    registration_number character varying,
    insurance_number character varying,
    company_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    expiry_date date,
    authorization_letter character varying,
    vehicle_type_id integer,
    user_id integer,
    latitude character varying,
    longitude character varying
);


ALTER TABLE public.vehicles OWNER TO junaid;

--
-- Name: vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: junaid
--

CREATE SEQUENCE public.vehicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehicles_id_seq OWNER TO junaid;

--
-- Name: vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: junaid
--

ALTER SEQUENCE public.vehicles_id_seq OWNED BY public.vehicles.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.active_storage_attachments ALTER COLUMN id SET DEFAULT nextval('public.active_storage_attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.active_storage_blobs ALTER COLUMN id SET DEFAULT nextval('public.active_storage_blobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.bids ALTER COLUMN id SET DEFAULT nextval('public.bids_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.companies ALTER COLUMN id SET DEFAULT nextval('public.companies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.company_informations ALTER COLUMN id SET DEFAULT nextval('public.company_informations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.driver_informations ALTER COLUMN id SET DEFAULT nextval('public.driver_informations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.individual_informations ALTER COLUMN id SET DEFAULT nextval('public.individual_informations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.locations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.shipment_vehicles ALTER COLUMN id SET DEFAULT nextval('public.shipment_vehicles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.shipments ALTER COLUMN id SET DEFAULT nextval('public.shipments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.vehicle_types ALTER COLUMN id SET DEFAULT nextval('public.vehicle_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.vehicles ALTER COLUMN id SET DEFAULT nextval('public.vehicles_id_seq'::regclass);


--
-- Data for Name: active_storage_attachments; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.active_storage_attachments (id, name, record_type, record_id, blob_id, created_at) FROM stdin;
\.


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.active_storage_attachments_id_seq', 1, false);


--
-- Data for Name: active_storage_blobs; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.active_storage_blobs (id, key, filename, content_type, metadata, byte_size, checksum, created_at) FROM stdin;
\.


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.active_storage_blobs_id_seq', 1, false);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-05-14 14:03:36.925189	2018-05-14 14:03:36.925189
\.


--
-- Data for Name: bids; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.bids (id, company_id, amount, status, detention, shipment_id, created_at, updated_at) FROM stdin;
3	4	250	2	12	12	2018-06-04 07:08:17.13085	2018-06-04 09:28:22.828728
4	4	1	3	1	12	2018-06-04 07:26:46.135787	2018-06-04 07:26:46.135787
2	3	25	3	25	12	2018-05-24 06:48:05.424716	2018-06-04 07:31:08.459779
1	1	2500	2	2500	11	2018-05-24 06:46:24.904626	2018-06-04 11:21:51.429485
6	4	6689.89999999999964	2	\N	17	2018-06-05 12:26:57.482423	2018-06-05 12:27:37.04318
7	4	13034.2000000000007	2	\N	18	2018-06-05 12:29:05.863632	2018-06-05 12:29:31.366932
8	4	2775	1	\N	19	2018-06-06 05:32:22.122795	2018-06-06 05:32:22.122795
9	4	2775	1	\N	19	2018-06-06 10:01:56.281817	2018-06-06 10:01:56.281817
10	4	3000	1	100	19	2018-06-07 06:31:12.25316	2018-06-07 06:31:12.25316
12	4	31875	1	100	21	2018-06-07 10:56:31.802632	2018-06-07 10:56:31.802632
13	4	31875	1	100	21	2018-06-07 10:56:37.559722	2018-06-07 10:56:37.559722
14	4	1000	2	250	13	2018-06-08 06:42:07.292503	2018-06-08 06:42:39.978907
11	4	29664.75	2	5588	20	2018-06-07 10:40:57.657956	2018-06-08 06:48:03.14394
16	4	6941.80000000000018	1	10	25	2018-06-12 09:52:51.183106	2018-06-12 09:52:51.183106
20	4	33100	1	11	22	2018-06-19 05:47:33.155572	2018-06-19 05:47:33.155572
21	4	7100	2	250	36	2018-06-19 07:22:52.192642	2018-06-19 07:23:39.63114
24	4	3512.69999999999982	2	50	37	2018-06-19 10:30:45.075704	2018-06-19 10:31:53.315542
25	4	2477.5	2	29	40	2018-06-19 11:38:36.877898	2018-06-19 11:38:49.896379
26	4	39200	2	12	39	2018-06-19 11:44:52.363675	2018-06-19 11:45:10.348166
27	4	3175	2	12	38	2018-06-19 11:48:45.01147	2018-06-27 09:10:22.124456
23	4	33100	3	1	22	2018-06-19 07:46:17.784162	2018-06-28 05:29:42.881956
17	4	885.450000000000045	3	\N	35	2018-06-19 04:54:59.240941	2018-06-19 11:47:05.117656
18	4	335.449999999999989	3	10	34	2018-06-19 05:46:24.862419	2018-06-19 05:46:24.862419
28	4	17868.0999999999985	2	10	42	2018-06-29 05:57:44.245583	2018-06-29 05:58:02.160863
22	4	329987.25	3	1	33	2018-06-19 07:45:56.001557	2018-06-19 07:45:56.001557
5	4	12	1	213	15	2018-06-04 11:32:32.556055	2018-06-04 11:32:51.183611
15	4	33100	3	13	22	2018-06-08 07:41:53.60533	2018-07-02 10:55:29.387861
19	4	6941.80000000000018	3	10	25	2018-06-19 05:46:55.797109	2018-07-02 12:39:21.098217
\.


--
-- Name: bids_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.bids_id_seq', 28, true);


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.companies (id, company_type, category, created_at, updated_at, is_contractual, verified) FROM stdin;
2	2	2	2018-05-24 10:05:06.712288	2018-05-24 10:05:06.712288	f	f
1	2	1	2018-05-15 06:51:13.766381	2018-05-28 09:09:04.606181	t	f
3	2	2	2018-05-24 10:07:53.241493	2018-06-04 05:55:09.968346	f	t
4	2	2	2018-06-04 06:55:25.491857	2018-06-04 06:55:25.491857	f	f
\.


--
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.companies_id_seq', 4, true);


--
-- Data for Name: company_informations; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.company_informations (id, name, address, landline_no, website_address, license_number, license_expiry_date, secondary_contact_name, secondary_mobile_no, news_update, created_at, updated_at, company_id, is_complete, avatar, documents) FROM stdin;
2	fleet Company	abc	3213213	Www.xyz.com	12412412	06/26/2018	sdf	sdas	f	2018-06-04 06:56:46.614753	2018-06-08 06:36:37.036891	4	t	\N	{}
1	sk Company	abc	21412		13243124214	21421421	03007283156	2423523523	t	2018-05-29 07:16:40.932243	2018-06-28 07:29:13.968187	1	t	\N	{}
\.


--
-- Name: company_informations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.company_informations_id_seq', 2, true);


--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.countries (id, name, dialing_code, process, created_at, updated_at, commision, tax, currency, penalty_free_hours, status) FROM stdin;
1	pakistan	+92	2	2018-05-28 09:23:36.51341	2018-06-28 12:11:22.344997	5	5	AED	24	\N
\.


--
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.countries_id_seq', 1, true);


--
-- Data for Name: driver_informations; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.driver_informations (id, national_id, expiry_date, license, license_expiry, user_id, created_at, updated_at, deleted_at) FROM stdin;
1	433434	\N	3434	\N	5	2018-06-06 10:15:15.102892	2018-06-06 10:15:15.102892	\N
2	Fhdgyg	\N	Fhfxhvvhxyr,4&&	\N	7	2018-06-08 06:50:55.853035	2018-06-08 06:50:55.853035	\N
3	21321421	\N	2432423	\N	8	2018-06-14 07:05:50.873888	2018-06-14 07:05:50.873888	\N
\.


--
-- Name: driver_informations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.driver_informations_id_seq', 3, true);


--
-- Data for Name: individual_informations; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.individual_informations (id, secondary_mobile_no, landline_no, national_id, expiry_date, news_update, created_at, updated_at, company_id, is_complete, avatar, documents) FROM stdin;
1	Gshshhs	Gshsh	Hzhsh	2018-06-08	t	2018-06-08 06:54:39.110942	2018-06-08 06:55:40.94933	4	f	"avatar.jpeg"	{}
\.


--
-- Name: individual_informations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.individual_informations_id_seq', 1, true);


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.locations (id, pickup_location, pickup_lat, pickup_lng, pickup_city, drop_location, drop_lat, drop_lng, drop_city, rate, created_at, updated_at, country_id) FROM stdin;
1	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	dubai	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	Ajman	1000.0	2018-05-28 09:59:55.957766	2018-05-28 10:14:26.332277	1
\.


--
-- Name: locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.locations_id_seq', 1, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.schema_migrations (version) FROM stdin;
20180412143338
20180423105614
20180423115521
20180423123911
20180423125951
20180424102059
20180424115400
20180502093957
20180503074144
20180509111822
20180509113125
20180509124521
20180509124545
20180516075628
20180511074144
20180516102347
20180516102408
20180517102251
20180518073818
20180524111717
20180528090535
20180528091710
20180523075212
20180528101357
20180528102928
20180529053756
20180531083354
20180531084527
20180531090541
20180531093244
20180531053259
20180531053325
20180531053332
20180604054859
20180605121838
20180605064113
20180605090454
20180605092926
20180606091043
20180606085656
20180606090647
20180606101018
20180607073011
20180611075956
20180607095049
20180612075938
20180612093213
20180619123419
20180620114815
20180621120542
20180626045527
20180702092606
20180702094405
\.


--
-- Data for Name: shipment_vehicles; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.shipment_vehicles (id, shipment_id, vehicle_id, created_at, updated_at, status) FROM stdin;
13	18	1	2018-06-11 08:45:31.88979	2018-06-11 08:45:31.88979	4
14	18	2	2018-06-11 08:45:31.891075	2018-06-11 08:45:31.891075	4
17	17	1	2018-06-19 05:49:20.101369	2018-06-19 05:49:20.101369	4
18	17	2	2018-06-19 05:49:20.102652	2018-06-19 05:49:20.102652	4
19	36	2	2018-06-19 07:24:26.638964	2018-06-19 07:24:26.638964	4
22	37	1	2018-06-19 10:44:10.68394	2018-06-19 10:44:10.68394	4
23	37	2	2018-06-19 10:44:10.685042	2018-06-19 10:44:10.685042	4
24	39	2	2018-06-19 11:45:31.375232	2018-06-19 11:45:31.375232	4
26	1	4	2018-06-27 11:00:46.139021	2018-06-27 11:00:46.139021	\N
25	40	2	2018-06-27 09:10:52.074476	2018-06-27 09:10:52.074476	4
21	37	4	2018-06-19 10:44:10.682689	2018-06-19 10:44:10.682689	2
27	42	1	2018-06-29 05:58:20.474506	2018-06-29 05:58:20.474506	4
28	42	2	2018-06-29 05:58:20.493297	2018-06-29 05:58:20.493297	6
29	38	1	2018-06-29 11:37:32.289565	2018-06-29 11:37:32.289565	\N
\.


--
-- Name: shipment_vehicles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.shipment_vehicles_id_seq', 29, true);


--
-- Data for Name: shipments; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.shipments (id, pickup_location, pickup_lat, pickup_lng, drop_location, drop_lat, drop_lng, pickup_date, pickup_time, loading_time, expected_drop_off, unloading_time, no_of_vehicles, cargo_description, cargo_packing_type, company_id, vehicle_id, approved, shipping_request_type_id, created_at, updated_at, state, amount, location_id, pickup_city, drop_city, country_id, process, vehicle_type_id, fleet_id, payment_option, cancel_by, cancel_penalty) FROM stdin;
10	Block T Gulberg 2, Lahore, Punjab, Pakistan	31.52025187579522	74.35666790957032	Mana Wala, Faisalabad, Punjab, Pakistan	31.457014349423652	73.152978334375	2018-05-15	2018-05-15 09:57:00	22	2018-05-15	3	2	abc	saturn	1	\N	f	\N	2018-05-15 09:58:19.24458	2018-05-31 09:42:35.958852	start	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
1	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-05-15	2018-05-15 05:05:00	4	2018-05-15	4	5	abc	ps	\N	\N	f	\N	2018-05-15 05:06:17.016949	2018-05-31 09:42:35.799423	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
2	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	2018-05-15	2018-05-15 05:08:00	5	2018-05-15	52	5	2	n64	\N	\N	f	\N	2018-05-15 05:09:20.746167	2018-05-31 09:42:35.834311	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
3	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	2018-05-15	2018-05-15 05:08:00	5	2018-05-15	52	5	2	n64	\N	\N	f	\N	2018-05-15 05:10:27.297522	2018-05-31 09:42:35.858247	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
4	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	2018-05-15	2018-05-15 05:08:00	5	2018-05-15	52	5	2	n64	\N	\N	f	\N	2018-05-15 05:10:42.881176	2018-05-31 09:42:35.87045	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
5	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	2018-05-15	2018-05-15 05:08:00	5	2018-05-15	52	5	2	n64	\N	\N	f	\N	2018-05-15 05:11:04.099827	2018-05-31 09:42:35.882312	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
7	Cantt, Lahore, Punjab, Pakistan	31.512997704527866	74.361153461438	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-05-15	2018-05-15 07:16:00	2	2018-05-15	2	4	abc	ps	\N	\N	f	\N	2018-05-15 07:16:29.595023	2018-05-31 09:42:35.894451	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
8	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-05-15	2018-05-15 09:25:00	1	2018-05-15	2	4	avca	n64	\N	\N	f	\N	2018-05-15 09:28:32.870907	2018-05-31 09:42:35.906331	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
9	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	PAF Falcon Complex Gulberg 3, Lahore, Punjab, Pakistan	31.4944936	74.3504881	2018-05-15	2018-05-15 09:29:00	2	2018-05-15	2	4	afa	n64	\N	\N	f	\N	2018-05-15 09:29:25.924891	2018-05-31 09:42:35.922727	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
6	Turkey - Dubai - United Arab Emirates	25.2280373	55.169491600000015	Trade Centre - Dubai - United Arab Emirates	25.2223231	55.283941599999935	2018-05-15	2018-05-15 05:08:00	5	2018-05-15	52	5	2	n64	1	\N	f	\N	2018-05-15 05:11:22.899289	2018-05-31 09:42:35.934855	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
14	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-05-29	2018-05-29 06:26:00	12	2018-05-29	21	4	abc	ps	1	\N	f	\N	2018-05-29 06:28:27.037983	2018-05-31 09:42:35.994703	posted	1000.0	1	dubai	Ajman	\N	\N	1	\N	\N	\N	\N
21	Shadman II Shadman, Lahore, Punjab, Pakistan	31.5310613	74.3333725	Shadman II Shadman, Lahore, Punjab, Pakistan	31.5310613	74.3333725	2018-06-07	2018-06-07 07:15:00	66	2018-06-07	66	5	sadf	boxes	1	\N	f	\N	2018-06-07 07:16:22.779004	2018-06-07 07:16:22.784715	posted	31875.0	\N	\N	\N	1	prorate	4	\N	\N	\N	\N
19	Liberty Market Gulberg III, Lahore, Punjab, Pakistan	31.511352300000002	74.3442561	Liberty Market Gulberg III, Lahore, Punjab, Pakistan	31.511352300000002	74.3442561	2018-06-01	2018-06-01 07:33:00	11	2018-06-01	12	3	af	boxes	1	\N	f	\N	2018-06-01 07:33:49.148522	2018-06-01 07:33:49.151741	posted	2775.0	\N	\N	\N	1	prorate	5	\N	\N	\N	\N
12	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-07-26	2018-05-16 11:34:00	3	2018-05-16	4	4	sgsgf	ps	1	\N	f	\N	2018-05-16 11:35:24.137936	2018-06-04 09:10:20.562475	posted	\N	\N	\N	\N	1	\N	1	\N	\N	\N	\N
11	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.514069699999997	74.35156479999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.514069699999997	74.35156479999999	2018-05-16	2018-05-16 10:06:00	6	2018-05-16	6	5	abc	n64	1	\N	f	\N	2018-05-16 10:06:53.286953	2018-06-04 11:25:30.441672	accepted	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	\N
13	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-05-29	2018-05-29 06:20:00	21	2018-12-29	12	4	abc	genesis	1	\N	f	\N	2018-05-29 06:21:16.218228	2018-06-05 12:10:57.986755	posted	1000.0	1	\N	\N	1	\N	1	\N	\N	\N	\N
23	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-08	2018-06-08 10:16:00	40	2018-06-08	40	5	abc	boxes	1	\N	f	\N	2018-06-08 10:17:24.553498	2018-06-08 10:17:24.557203	posted	18875.0	\N	\N	\N	1	prorate	5	\N	30	\N	\N
24	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	Islamabad, Islamabad Capital Territory, Pakistan	33.6844202	73.04788480000002	2018-06-12	2018-06-12 06:15:00	21	2018-06-12	2134	4	sdvasc	drums	1	\N	f	\N	2018-06-12 06:18:01.103702	2018-06-12 06:18:01.11192	posted	452949.4	0	Karachi	Islamabad	1	prorate	5	\N	30_days_credit_period	\N	\N
25	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-06-12	2018-06-12 06:27:00	21	2018-06-12	13	4	abc	boxes	1	\N	f	\N	2018-06-12 06:28:34.56903	2018-06-12 06:28:34.57644	posted	6941.8	\N	dubai	Ajman	1	prorate	4	\N	bank_remittance	\N	\N
26	Hasnainabad, Lahore, Punjab, Pakistan	31.515532600000004	74.3605	Block B 1 Gulberg III, Lahore, Punjab, Pakistan	31.51625	74.35361539999997	2018-06-13	2018-06-13 06:20:00	40	2018-06-13	88	5	jgf	boxes	1	\N	f	\N	2018-06-13 06:20:11.919381	2018-06-13 06:20:11.925076	posted	30891.5	\N	\N	Lahore	1	prorate	5	\N	after_delivery	\N	\N
20	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Thokar Niaz Baig Flyover, Lahore, Punjab, Pakistan	31.47104509999999	74.24154020000003	2018-06-06	2018-06-06 07:55:00	45	2018-06-06	77	5	g	sacks	1	\N	f	\N	2018-06-06 07:56:29.604288	2018-06-13 06:15:53.089919	cancel	29664.75	\N	\N	Lahore	1	prorate	3	\N	credit_card	cargo_owner	0.0
27	Hasnainabad, Lahore, Punjab, Pakistan	31.515532600000004	74.3605	Hasnainabad, Lahore, Punjab, Pakistan	31.515532600000004	74.3605	2018-06-13	2018-06-13 11:39:00	45	2018-06-13	45	4	45	drums	1	\N	f	\N	2018-06-13 06:40:13.249125	2018-06-13 06:40:13.255138	posted	17100.0	\N	\N	\N	1	prorate	1	\N	30_days_credit_period	\N	\N
17	Block Q Gulberg 2, Lahore, Punjab, Pakistan	31.525343999999997	74.35287079999999	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-05-31	2018-05-31 10:55:00	18	2018-05-31	17	1	abc	drums	1	\N	f	\N	2018-05-31 11:01:25.33546	2018-06-19 05:49:20.10389	vehicle_assigned	6689.9	\N	\N	\N	1	prorate	6	4	\N	\N	\N
18	Gulberg V, Lahore, Punjab, Pakistan	31.536171473832393	74.3551023979004	Islamabad, Islamabad Capital Territory, Pakistan	33.6844202	73.04788480000002	2018-06-01	2018-06-01 06:44:00	50	2018-06-01	58	2	abc	sacks	1	\N	f	\N	2018-06-01 06:48:15.737526	2018-06-26 12:39:15.38439	ongoing	13034.2	\N	\N	\N	1	prorate	5	4	0	\N	\N
15	Askari 5, Lahore, Punjab, Pakistan	31.497284099999995	74.3440957	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-05-31	2018-05-31 10:39:00	25	2018-05-31	23	4	abc	sacks	1	\N	f	\N	2018-05-31 10:39:50.420252	2018-07-02 10:42:14.032849	posted	\N	\N	\N	\N	1	bidding	2	4	Bank Remittance	fleet_owner	\N
28	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Hasnainabad, Lahore, Punjab, Pakistan	31.515532600000004	74.3605	2018-06-13	2018-06-13 11:42:00	23	2018-06-13	23	3	avc	boxes	1	\N	f	\N	2018-06-13 06:45:01.333096	2018-06-13 06:54:19.394734	posted	6236.1	\N	\N	\N	1	prorate	4	\N	bank_remittance	\N	\N
29	Hasnainabad, Lahore, Punjab, Pakistan	31.515532600000004	74.3605	Madina Colony, Lahore, Punjab, Pakistan	31.495718600000004	74.3601334	2018-06-13	2018-06-13 06:57:53.151	40	2018-06-13	39	2	47	sacks	1	\N	f	\N	2018-06-13 06:57:54.080174	2018-06-13 06:58:20.583979	posted	7472.0	\N	\N	\N	1	prorate	3	\N	30_days_credit_period	\N	\N
30	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-13	2018-06-13 08:02:00	3	2018-06-13	1	1	3	palletized	1	\N	f	\N	2018-06-13 08:03:29.284575	2018-06-13 08:03:29.290919	posted	-25.0	\N	\N	\N	1	prorate	1	\N	credit_card	\N	\N
31	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-13	2018-06-13 08:04:00	29	2018-06-13	1	1	\n3	palletized	1	\N	f	\N	2018-06-13 08:04:44.773622	2018-06-13 08:04:44.777261	posted	1275.0	\N	\N	\N	1	prorate	1	\N	credit_card	\N	\N
41	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-06-13	2018-06-13 02:40:00	9	2018-06-13	8	1	8	palletized	\N	\N	f	\N	2018-06-21 07:06:27.342758	2018-06-21 07:06:27.342758	cancel	885.45	1	dubai	Ajman	1	prorate	1	4	credit_card	fleet_owner	75.0
37	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Lahore, Punjab, Pakistan	31.52036960000001	74.3587473	2018-06-30	2018-06-19 11:30:00	5	2018-06-30	6	6	hello	containerised	1	\N	f	\N	2018-06-19 10:28:45.214407	2018-06-29 06:07:43.71777	ongoing	3512.7	\N	dubai	Lahore	1	prorate	6	4	after_delivery	\N	\N
33	Madina Colony, Lahore, Punjab, Pakistan	31.495718600000004	74.3601334	Arizona, USA	34.0489281	-111.09373110000001	2018-06-13	2018-06-13 13:44:00	32	2018-06-13	22	5	3r	drums	1	\N	f	\N	2018-06-13 08:45:48.621244	2018-06-13 08:45:48.624241	posted	329987.25	\N	\N		1	prorate	5	\N	credit_card	\N	\N
34	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-06-13	2018-06-13 08:56:10.614	3	2018-06-13	3	1	3	palletized	1	\N	f	\N	2018-06-13 08:56:11.318218	2018-06-13 08:56:11.321238	posted	335.45	1	dubai	Ajman	1	prorate	1	\N	credit_card	\N	\N
35	Dubai Marina - Dubai - United Arab Emirates	25.0805422	55.14034259999994	Ajman - United Arab Emirates	25.40521649999999	55.51364330000001	2018-06-13	2018-06-13 02:40:00	9	2018-06-13	8	1	8	palletized	1	\N	f	\N	2018-06-13 09:41:30.155787	2018-06-21 07:06:27.351799	posted	885.45	1	dubai	Ajman	1	prorate	1	4	credit_card	fleet_owner	\N
22	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-08	2018-06-08 07:41:00	100	2018-06-08	70	4	abc	sacks	1	\N	f	\N	2018-06-08 07:41:28.803114	2018-06-13 10:30:00.668399	posted	33100.0	\N	\N	\N	1	prorate	5	4	0	fleet_owner	300.0
42	Plot 70, Block B2 Block B 2 Gulberg III, Lahore, Punjab, Pakistan	31.5107498	74.3522752	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-06-29	2018-06-29 05:44:00	30	2018-06-29	50	2	abc	other	1	\N	f	\N	2018-06-29 05:46:34.935657	2018-06-29 07:41:09.37718	completed	17868.1	\N	\N	\N	1	prorate	2	4	bank_remittance	\N	\N
36	Plot 69, Block B2 Block B 2 Gulberg III, Lahore, Punjab, Pakistan	31.5103544	74.3524707	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-06-19	2018-06-19 07:17:00	20	2018-06-19	20	4	abc	palletized	1	\N	f	\N	2018-06-19 07:19:10.321464	2018-06-22 10:54:55.530834	vehicle_assigned	7100.0	\N	\N	Karachi	1	prorate	3	4	BankRemittance	\N	\N
32	Madina Colony, Lahore, Punjab, Pakistan	31.495718600000004	74.3601334	Madina Colony, Lahore, Punjab, Pakistan	31.495718600000004	74.3601334	2018-06-13	2018-06-13 22:07:00	1	2018-06-13	1	1	d	drums	1	\N	f	\N	2018-06-13 08:08:12.594642	2018-06-28 11:18:33.933715	posted	-125.0	\N	\N	\N	1	prorate	2	\N	after_delivery	\N	\N
38	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-19	2018-06-19 11:16:00	18	2018-06-19	50	1	abc	sacks	1	\N	f	\N	2018-06-19 11:16:33.038293	2018-06-29 11:38:34.374652	ongoing	3175.0	\N	\N	\N	1	prorate	4	4	30 days Credit period	\N	\N
39	London, UK	51.5073509	-0.12775829999998223	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-06-19	2018-06-19 11:19:00	10	2018-06-19	8	1	abc	palletized	1	\N	f	\N	2018-06-19 11:19:51.424977	2018-06-26 07:24:30.882695	ongoing	39200.0	\N	\N	Karachi	1	prorate	1	4	Pay at origin during shipment pickup	\N	\N
40	Block A1 Gulberg III, Lahore, Punjab, Pakistan	31.512741600367733	74.35793481062012	Block B1 Gulberg III, Lahore, Punjab, Pakistan	31.5140587	74.35287079999999	2018-06-19	2018-06-19 11:32:00	22	2018-06-19	32	1	af	sacks	1	\N	f	\N	2018-06-19 11:34:22.222829	2018-06-27 09:38:25.654736	ongoing	2477.5	\N	\N	\N	1	prorate	3	4	before_pick_up	\N	\N
43	Askari 5, Lahore, Punjab, Pakistan	31.497284099999995	74.3440957	Karachi, Karachi City, Sindh, Pakistan	24.8607343	67.00113639999995	2018-05-31	2018-05-31 10:39:00	25	2018-05-31	23	4	abc	sacks	\N	\N	f	\N	2018-07-02 10:42:14.024188	2018-07-02 10:42:14.028726	cancel	29304.2	\N	\N	\N	1	prorate	2	4	Bank Remittance	fleet_owner	300.0
\.


--
-- Name: shipments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.shipments_id_seq', 43, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, first_name, last_name, mobile, company_id, role, mobile_verified, verification_code, confirmation_token, confirmed_at, confirmation_sent_at, terms_accepted, country_id, device_id, os, deleted_at) FROM stdin;
8		$2a$11$giRw3uflte95HK2f2RPZaOt.VLrMjA2CSqeJyDfcIl5O9EbIo9tf6	\N	\N	\N	0	\N	\N	\N	\N	2018-06-14 07:05:50.842445	2018-06-14 07:05:50.872456	s	k	+923007283156	4	7	verified		zKcLX1cFZjG1sEccJv6S	\N	2018-06-14 07:05:50.842603	f	1	\N	\N	\N
1	sohail@gmail.com	$2a$11$ojs8E1uSTtg1vf5dJeN/2eb3bvDl8dR8/SF4eJbz99WaJguiI/Xs.	\N	\N	\N	50	2018-07-02 11:18:50.845053	2018-07-02 10:50:11.40462	203.215.181.62	203.215.173.211	2018-05-15 05:01:03.707605	2018-07-02 11:18:50.84585	sohail	khalil	03007283156	1	3	verified	4myg	\N	2018-05-24 05:58:41.336691	\N	f	1	cJ0zPblOjvM:APA91bFLQUdtw_qGJ3gmWdH7S8b8nIR5w_7iv04pB_mImZ2uOrjb3fwp-XPeqTROV30fnO_XP5eg_QetXTXlMP88oIfeluw_9iYFVn8ld5LQ4ZUiq-CvXlE8Je836hqewBrDcRgAIMKzsI30hzn9J4esHxsIg534BA	android	\N
4	fleet1@gmail.com	$2a$11$4aEV7a9Xsyoy19eBGKkZW.5j7.HIFLqW5iNTBpFZFJXy4jQ.WI0uu	\N	\N	\N	35	2018-07-02 12:01:35.982831	2018-07-02 12:01:05.373462	202.166.163.179	202.166.163.179	2018-06-04 06:55:16.144556	2018-07-02 12:01:35.983661	fleet	1	+923007283156	4	4	verified	3785	g4_yLSo9-8z-WxaE5uc3	\N	2018-06-04 06:55:16.144715	f	1	\N	\N	\N
9	sohail1@gmail.com	$2a$11$TfXDFmTuLx8USFWF/4nKkObF7fjccMQdd0dKQElxvQ0CcoicG3w76	\N	\N	\N	1	2018-06-29 10:10:45.697651	2018-06-29 10:10:45.697651	203.215.173.211	203.215.173.211	2018-06-29 10:10:34.967967	2018-06-29 10:10:45.698475	abc	sk	+921232142421	\N	\N	error	2759	BunGPwpr-87BzqFwsfRw	\N	2018-06-29 10:10:34.96813	t	1			\N
6	richard.sunny@virtual-force.com	$2a$11$sWejxrAxvnfFa2Tw0Q3lO.TT2c3CPmlzFWrgkpGIY2/VWOJzD6Psy	\N	\N	\N	1	2018-06-08 06:25:30.870521	2018-06-08 06:25:30.870521	203.215.181.62	203.215.181.62	2018-06-08 06:25:23.299775	2018-06-08 06:25:30.87217	Richard	Sunny	+923008032610	\N	\N	error	5676	8PZyuSrZ6vHZ6nETZed3	\N	2018-06-08 06:25:23.299929	t	1	\N	\N	\N
7		$2a$11$U7Y8Ifh1wUMB/KwnOQ8kVuYUbfr0/X/Jmp98FUAyHsVqzzsZfFWJK	\N	\N	\N	0	\N	\N	\N	\N	2018-06-08 06:50:55.849648	2018-06-22 13:07:55.493097	\N	\N	+923007283156	1	7	verified		i1H9J6miwn8E2hpD1_1-	\N	2018-06-08 06:50:55.849752	f	1	\N	\N	\N
3	admin@example.com	$2a$11$KwNYIwN/Qwq0HZSEOqkhiOipb7POxqc5j2B7QTryz6b/o7072sMIi	\N	\N	\N	10	2018-05-29 07:06:10.408096	2018-05-29 07:05:51.436158	127.0.0.1	127.0.0.1	2018-05-28 09:26:28.203489	2018-05-29 07:06:10.409089	\N	\N	\N	\N	2	verified		xyUwRzcZJZG-7BDwfsV2	2018-05-28 09:27:12.368431	2018-05-28 09:26:28.203679	f	\N	\N	\N	\N
2	fleet@gmail.com	$2a$11$SrDl9QkSSdSFyX6KUcMwMuOm0akWJWn61ZS4ul61Myz9Tsca.3fSq	\N	\N	\N	8	2018-06-26 10:35:01.435548	2018-06-07 10:13:37.208142	203.215.173.211	203.215.181.62	2018-05-24 09:46:30.645487	2018-06-26 10:35:01.436654	f 	fleet	+923244009045	3	4	verified		LZixcJwmcNAfsz4JJhQC	\N	2018-05-24 09:46:30.645629	f	\N	\N	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.users_id_seq', 9, true);


--
-- Data for Name: vehicle_types; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.vehicle_types (id, code, name, country_id, fleet_penalty_amount, cargo_penalty_amount, base_rate, free_kms, rate_per_km, waiting_charges, load_unload_free_hours, created_at, updated_at) FROM stdin;
1	\N	Dyna Dry Pickup (Open Top) – 2 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:11:58.48657	2018-06-12 07:52:55.308927
2	\N	Dyna Dry Pickup (Covered Top) – 2 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:12:21.778289	2018-06-12 07:52:55.331284
3	\N	Dyna Dry Pickup (Open Top) – 5 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:12:38.898393	2018-06-12 07:52:55.342482
4	\N	Dyna Dry Pickup (Open Top) – 10 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:12:49.930818	2018-06-12 07:52:55.354932
5	\N	Dyna Reefer Pickup – 5 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:13:10.027128	2018-06-12 07:52:55.367
6	\N	Dyna Reefer Pickup – 10 Tons	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:13:28.187307	2018-06-12 07:52:55.37907
7	\N	20ft Container Truck & Trailer (22 Tons)	1	20.0	75.0	50.0	5	5.0	50.0	5	2018-05-31 09:13:46.57181	2018-06-12 07:52:55.390657
\.


--
-- Name: vehicle_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.vehicle_types_id_seq', 7, true);


--
-- Data for Name: vehicles; Type: TABLE DATA; Schema: public; Owner: junaid
--

COPY public.vehicles (id, registration_number, insurance_number, company_id, created_at, updated_at, expiry_date, authorization_letter, vehicle_type_id, user_id, latitude, longitude) FROM stdin;
3	Fhyggbsj	4662727	1	2018-06-08 06:49:11.156126	2018-06-08 06:49:11.156126	2018-06-08	Dhvajabakakb	2	\N	\N	\N
2	lee1243	12345	4	2018-06-08 06:06:41.203917	2018-06-28 10:34:05.987302	2018-06-08	1335435	4	8	31.5140587	74.35287079999999
4	737373	Hdhd7373	4	2018-06-19 10:34:52.052735	2018-06-29 06:16:34.536137	2018-06-22	Hdhdhdh	5	8	31.510491	74.3520815
1	655	656	4	2018-06-06 10:15:51.12836	2018-06-29 07:32:25.755108	2018-06-06	5656	4	8	31.5103546	74.3524705
\.


--
-- Name: vehicles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: junaid
--

SELECT pg_catalog.setval('public.vehicles_id_seq', 4, true);


--
-- Name: active_storage_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT active_storage_attachments_pkey PRIMARY KEY (id);


--
-- Name: active_storage_blobs_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.active_storage_blobs
    ADD CONSTRAINT active_storage_blobs_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: bids_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.bids
    ADD CONSTRAINT bids_pkey PRIMARY KEY (id);


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: company_informations_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.company_informations
    ADD CONSTRAINT company_informations_pkey PRIMARY KEY (id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: driver_informations_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.driver_informations
    ADD CONSTRAINT driver_informations_pkey PRIMARY KEY (id);


--
-- Name: individual_informations_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.individual_informations
    ADD CONSTRAINT individual_informations_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: shipment_vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.shipment_vehicles
    ADD CONSTRAINT shipment_vehicles_pkey PRIMARY KEY (id);


--
-- Name: shipments_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.shipments
    ADD CONSTRAINT shipments_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vehicle_types_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.vehicle_types
    ADD CONSTRAINT vehicle_types_pkey PRIMARY KEY (id);


--
-- Name: vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: junaid
--

ALTER TABLE ONLY public.vehicles
    ADD CONSTRAINT vehicles_pkey PRIMARY KEY (id);


--
-- Name: index_active_storage_attachments_on_blob_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_active_storage_attachments_on_blob_id ON public.active_storage_attachments USING btree (blob_id);


--
-- Name: index_active_storage_attachments_uniqueness; Type: INDEX; Schema: public; Owner: junaid
--

CREATE UNIQUE INDEX index_active_storage_attachments_uniqueness ON public.active_storage_attachments USING btree (record_type, record_id, name, blob_id);


--
-- Name: index_active_storage_blobs_on_key; Type: INDEX; Schema: public; Owner: junaid
--

CREATE UNIQUE INDEX index_active_storage_blobs_on_key ON public.active_storage_blobs USING btree (key);


--
-- Name: index_company_informations_on_company_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_company_informations_on_company_id ON public.company_informations USING btree (company_id);


--
-- Name: index_driver_informations_on_deleted_at; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_driver_informations_on_deleted_at ON public.driver_informations USING btree (deleted_at);


--
-- Name: index_individual_informations_on_company_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_individual_informations_on_company_id ON public.individual_informations USING btree (company_id);


--
-- Name: index_shipments_on_company_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_shipments_on_company_id ON public.shipments USING btree (company_id);


--
-- Name: index_shipments_on_shipping_request_type_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_shipments_on_shipping_request_type_id ON public.shipments USING btree (shipping_request_type_id);


--
-- Name: index_shipments_on_vehicle_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_shipments_on_vehicle_id ON public.shipments USING btree (vehicle_id);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: junaid
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON public.users USING btree (confirmation_token);


--
-- Name: index_users_on_deleted_at; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_users_on_deleted_at ON public.users USING btree (deleted_at);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: junaid
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: index_vehicles_on_company_id; Type: INDEX; Schema: public; Owner: junaid
--

CREATE INDEX index_vehicles_on_company_id ON public.vehicles USING btree (company_id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

