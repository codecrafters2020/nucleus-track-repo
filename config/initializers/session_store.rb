Rails.application.config.session_store :cookie_store, key: '_lorryz_backend', domain: {
  production: '.lorryz.com',
  development: '.lvh.me'
}.fetch(Rails.env.to_sym, :all)