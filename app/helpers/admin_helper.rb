module AdminHelper
  def company_details_column_names fleet_owner
    columns= ["full_name","email", "mobile", "Country","Company Name","License","Verified","Featured","Blacklisted","Safe for cash on delivery","Actions"]
    columns.delete("Safe for cash on delivery") if fleet_owner
    create_thead columns

  end

  def individual_details_column_names fleet_owner
    columns = ["full_name","email", "mobile", "Country","Emirates ID","Expiry Date","Verified","Featured","Blacklisted","Safe for cash on delivery","Actions"]
    columns.delete("Safe for cash on delivery") if fleet_owner
    create_thead columns
  end

  def create_thead columns
    str = " <thead> <tr>"
    columns.map do |col|
      str = str + " <th> #{col.humanize} </th> "
    end
    str + "</tr> </thead>"
    str.html_safe
  end

end
