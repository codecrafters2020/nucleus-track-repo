module ApplicationHelper
	def bootstrap_class_for_flash(flash_type)
    case flash_type
    when 'success'
      'alert-success'
    when 'error'
      'alert-danger'
    when 'alert'
      'alert-warning'
    when 'notice'
      'alert-info'
    else
      flash_type.to_s
    end
  end

	def slider_range_value(shipment_vehicle)
		if shipment_vehicle.status == "booked"
			0
		elsif shipment_vehicle.status == 'start'
			20
		elsif shipment_vehicle.status == 'loading'
			35
		elsif shipment_vehicle.status == 'enroute'
			55
		elsif shipment_vehicle.status == 'unloading'
			77
		elsif shipment_vehicle.status == 'finish'
			92
		end 
	end

	def signup?
		params[:controller] == "companies" and params[:action] == "get_company_information"
	end

	def format_amount_to_currency amount  , unit = "" , precision = 2
		return "#{ number_to_currency(amount, precision: precision, unit: unit)}"
	end
	def format_float number
		return "#{ number_with_precision(number, precision: 2,strip_insignificant_zeros: true)}"
	end
	def shipment_currency shipment
		shipment.try(:country).try(:currency).to_s rescue ""
	end
	def user_currency
		current_user.try(:country).try(:currency).to_s rescue ""
	end


	def shipment_states
		Shipment.states.map(&:clone).delete_if{|element| element ==["Initial","initial"]}
	end

	def call_geolocate
		return "geolocate(this)"
		# if current_user.present? and current_user.country.present?
		# 	"geolocate(this ,'#{current_user.country.short_name.downcase}')" if current_user.present?
		# else
		# 	"geolocate(this)"
		# end
	end

	def display_date_format
		"%d-%m-%Y"
	end

	def excel_date_format
		"%d-%m-%Y"
	end

	def excel_datetime_format
		' %d-%m-%Y  %I:%M %p '
	end

	def activity_date_format
		"%d-%m-%Y  %I:%M %p"
	end

	def local_time_format datetime
		local_time(datetime, "%I:%M %p") rescue ""
	end

	def assigned_vehicle_for_driver(shipment)
		if @user.vehicle
			shipment.shipment_vehicles.find_by_vehicle_id @user.vehicle.id
		end
	end

	def local_datetime_format datetime
		local_time(datetime, "%d-%m-%Y %l:%M%P") rescue ""
	end

	def local_datetime_excel_format datetime
		local_time(datetime, excel_datetime_format) rescue ""
	end

	def convert_to_shipment_country_timezone country, datetime
		tz = ISO3166::Country.new(country.short_name).timezones.zone_identifiers.first
		return datetime.in_time_zone(tz) rescue (return datetime)
	end
	

	def action_date(activity)
		# "#{activity.created_at.strftime("%m/%d/%Y %I:%M%p")}" rescue ''
		local_time(activity.created_at, "%d-%m-%Y %l:%M%P") rescue ''
	end

	def rebid s
		s.bids.pluck(:company_id).include? current_user.company.id
	end

	def entity_name(activity)

		# if activity.shipment_vehicle?
		# 	@link_id = activity.auditable.shipment.id rescue ''
		# elsif activity.bid?
		# 	@link_id = activity.auditable.shipment_id
		# else
		# 	@link_id = activity.auditable.id
		# end
		if activity.shipment?
			"#{activity.auditable_type} ##{link_to "#{activity.auditable.id}", admin_shipment_path(activity.auditable.id) }".html_safe rescue ''	
		elsif activity.user?
			"#{activity.auditable_type} ##{link_to "#{activity.auditable.id}", admin_user_path(activity.auditable.id) }".html_safe rescue ''	
		else
			"#{activity.auditable_type}".html_safe rescue ''	
		end
		
	end

	def display_bids_on_cargo_shipment_status
		["ongoing", "cancel" , "completed" ,"upcoming" , "accepted"]
	end

	def cargo_pay_to_lorryz shipments
		(shipments.where(state: "completed").sum(:amount).to_f rescue 0.0) + (shipments.where(state: "cancel").sum(:cancel_penalty).to_f rescue 0.0)
	end



	def fleet_current_receivable_payments shipments
		payable = shipments.where(state: "completed", lorryz_comission_received: false, payment_option: ["before_pick_up","after_delivery"]).sum(:lorryz_share_amount).to_f #rescue 0.0
		cancel_payable = (shipments.where(state: "cancel", cancel_by: "fleet_owner").sum(:cancel_penalty).to_f) #	rescue 0.0
		receivable  = (shipments.where(state: "completed", paid_to_fleet: false, payment_option: ["credit_card","bank_remittance","credit_period"]).sum(:fleet_income).to_f ) #rescue 0.0
		return (  receivable - (payable + cancel_payable) )  #rescue 0.0
	end

	def fleet_current_payable_payments shipments
		payable = shipments.where(state: "completed", lorryz_comission_received: false, payment_option: ["before_pick_up","after_delivery"]).sum(:lorryz_share_amount).to_f  #rescue 0.0
		cancel_payable = (shipments.where(state: "cancel", cancel_by: "fleet_owner").sum(:cancel_penalty).to_f) #	rescue 0.0
		receivable  = (shipments.where(state: "completed", paid_to_fleet: false, payment_option: ["credit_card","bank_remittance","credit_period"]).sum(:fleet_income).to_f ) #rescue 0.0
		return (  (payable + cancel_payable) - receivable )  #rescue 0.0
	end

	def rated_by company
		rating_count  = company.received_ratings.count
		if rating_count > 0
			return "(" + rating_count.to_s + ")"
		else
			"(0)"
		end
	end

end
