class ShipmentMailer < ApplicationMailer

	default from: 'Lorryz <no-reply@lorryz.com>'

	def change_destination_mail_to_admin(shipment, old_destination, new_destination)
		@shipment = shipment
		@old_destination = old_destination
		@new_destination = new_destination
		mail(to: "contact@lorryz.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end

	def change_destination_approved_mail(shipment, old_destination)
		@shipment = shipment
		@old_destination = old_destination
		shipment_users
		subject = "Lorryz - Shipment Change Destination Request Approved"
		mail(to: [@cargo_owner_email, @fleet_owner_email], subject: subject) if @cargo_owner_email.present? or @fleet_owner_email.present?
		# mail(to: "sohail.khalil@virtual-force.com", subject: "Request received for change in destination for #{@shipment.try(:id)}")
	end

	def shipment_canceled shipment_id
		@shipment = Shipment.find(shipment_id)
		shipment_users
		subject = "Lorryz - Shipment Cancelled"
		mail(to: ["contact@lorryz.com", @cargo_owner_email, @fleet_owner_email], subject: subject)
	end

	def shipment_rating shipment_id, receiver_id
		@shipment = Shipment.find(shipment_id)
		@email = User.find(receiver_id).email
		subject = "Lorryz - Your Shipment has been Rated."
		mail(to: @email , subject: subject)
	end

	def bidless_shipment shipment_id
		@shipment = Shipment.find(shipment_id)

		@online_users = find_all_online_users

		subject = "No bids received in 90s – Shipment No : " + shipment_id.to_s
		mail(to: ["contact@lorryz.com", "hashmi@lorryz.com", "meherwer.09@gmail.com", "hammadt11@gmail.com"], subject: subject)
	end

	def no_bids_accepted shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Customer did not accept bids in 180s – Shipment No : #{shipment_id}"
		mail(to: ["contact@lorryz.com", "hashmi@lorryz.com", "meherwer.09@gmail.com","hammadt11@gmail.com"], subject: subject)
	end

	def shipment_not_started shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Driver did not start shipment in 120s – Shipment No : #{shipment_id} "
		mail(to: ["contact@lorryz.com", "hashmi@lorryz.com", "meherwer.09@gmail.com","hammadt11@gmail.com"], subject: subject)
	end


	def shipment_started shipment_id
		@shipment = Shipment.find(shipment_id)
		#shipment_users
		subject = "Track ongoing shipment – Shipment No : " + shipment_id.to_s
		mail(to: ["contact@lorryz.com", "hashmi@lorryz.com", "meherwer.09@gmail.com","hammadt11@gmail.com"], subject: subject)
	end

	private
	def shipment_users
		@fleet_owner_email = @shipment.fleet.fleet_owner.email rescue ""
		@cargo_owner_email = @shipment.company.cargo_owner.email rescue ""
	end

	private
	def find_all_online_users
		#shipment = Shipment.find(params[:shipment_id])
		online_users = User.individual_fleet_owners
		online_users.each do |user|
			unless (user.is_online == true && user.country_id == @shipment.country_id)
				online_users -= [user]
			end
		end
		return online_users
	end

end
