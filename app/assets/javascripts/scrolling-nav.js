//jQuery for page scrolling feature - requires jQuery Easing plugin
jQuery(document).ready(function($){  
var headerHeight = jQuery(".header-holder").height();
jQuery(function() {
    jQuery(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = jQuery(this);
        jQuery('html, body').stop().animate({
            scrollTop: jQuery($anchor.attr('href')).offset().top - headerHeight
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});
});

