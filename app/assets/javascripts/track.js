$(document).ready(function () {

    jQuery('.navi-icon').click(function () {

        $(this).toggleClass('open');
    });
    jQuery(".right-menu").click(function () {
        $("#c-mask-right").fadeToggle();
        $(".navigation-holder").toggleClass("open-left-menu");
        $(".header-holder").toggleClass("header-active");
        $("body").toggleClass('disable-scroll');
    });
    jQuery("#c-mask-right").click(function () {
        $(this).fadeOut();
        $(".navi-icon").removeClass("open");
        $(".navigation-holder").removeClass("open-left-menu");
        $("body").removeClass('disable-scroll');
        $(".header-holder").removeClass("header-active");
    });

    //sticky header script //
    var headerHeight = $(".header-holder").height();
    jQuery('.header-holder').stickMe({
        transitionDuration: 300,
        shadow: true,
        shadowOpacity: 0.5,
        animate: true,
        triggerAtCenter: true,
        transitionStyle: 'fade',
        stickyAlready: true,
        topOffset: 200,
    });

    // range slider //
    $(".range-pro").each(function (index, element) {
        var values = $(this).val();
        $(this).prev().css("width", values + "%");
        $(this).next().css("left", values + "%");
    });




});

function changeState(el) {
    if (el.readOnly)
        el.checked = el.readOnly = false;
    else if (!el.checked)
        el.readOnly = el.indeterminate = true;
}
// top navigation scroll to ID //
(function ($) {
    jQuery(window).on("load", function ($) {

        /* Page Scroll to id fn call */
        jQuery("ul.navigation a,a[href='#top'],a[rel='m_PageScroll2id']").mPageScroll2id({
            highlightSelector: "ul.navigation a"
        });

        /* demo functions */
        jQuery("a.page-scroll[rel='next']").click(function (e) {
            e.preventDefault();
            var to = $(this).parent().parent("section").next().attr("id");
            $.mPageScroll2id("scrollTo", to);
        });

    });
})(jQuery);