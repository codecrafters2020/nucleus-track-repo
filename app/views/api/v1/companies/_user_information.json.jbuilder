json.user do
	json.extract! user, :id,:first_name,:last_name,:email,:country_id,:mobile,:company_id,:role,:mobile_verified,:terms_accepted,:device_id,:os,:is_online
	json.company user.company
	json.country user.country
	json.vehicle_shipments user.vehicle.shipment_vehicles if user.vehicle
	json.is_max_period_amount_overdue user.company.is_max_period_amount_overdue? if user.company
	json.auth_token AuthToken.issue_token({ user_id: user.id }) if user
	json.process user.country.process if user.country
	json.information user.company ? (user.company.individual? ? user.company.individual_information : user.company.company_information) : nil
	json.average_rating  user.company ? (user.company.average_rating ? user.company.average_rating : 0) : 0
	json.rated_count  user.company ? (user.company.received_ratings.count ? user.company.received_ratings.count : 0) : 0
	json.notification_count user.unread_notifications.count
end
json.success msg