json.shipments do
	json.array! @shipments do |shipment|
		
			json.extract! shipment, :id, :pickup_location, :drop_location, :pickup_date, :expected_drop_off,:state
			json.amount number_with_precision(shipment.amount.to_f, precision: 2)
			json.payment_option shipment.try(:payment_option).try(:camelcase)
			json.pickup_time shipment.pickup_time
			json.vehicle_type shipment.try(:vehicle_type).try(:name)
			json.cancel_charges shipment.cancel_penalty
			json.company_id shipment.company_id
			json.fleet_id shipment.fleet_id
	end
end
json.total_amount @shipments.where(state: "completed").sum(:amount).to_f + @shipments.where(state: "cancel").sum(:cancel_penalty).to_f

# json.tax number_with_precision(@shipments.sum(:amount_tax).to_f, precision: 2)
json.receivable number_with_precision(@shipments.where(state: "completed", paid_to_fleet: false, payment_option: ["credit_period"]).sum(:fleet_income).to_f, precision: 2)
json.cancel_payable @shipments.where(state: "cancel").sum(:cancel_penalty).to_f