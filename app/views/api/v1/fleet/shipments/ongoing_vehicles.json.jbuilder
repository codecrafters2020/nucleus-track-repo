json.array! @shipment_vehicles do |sv|
  json.id sv.vehicle.id
  json.registration_number sv.vehicle.registration_number
  json.vehicle_type sv.vehicle.try(:vehicle_type).try(:name)
	json.driver_name sv.vehicle.try(:driver).try(:full_name)
  json.driver_mobile sv.vehicle.try(:driver).try(:mobile)
  json.latitude sv.vehicle.latitude
  json.longitude sv.vehicle.longitude
  json.pickup_location sv.shipment.pickup_location
  json.drop_location sv.shipment.drop_location
  json.shipment_id sv.shipment.id
end