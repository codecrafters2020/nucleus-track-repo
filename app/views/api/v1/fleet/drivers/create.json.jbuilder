json.extract! @shipment, :id, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng,
 :pickup_date, :no_of_vehicles, :loading_time, :expected_drop_off, :unloading_time, 
 :cargo_description, :cargo_packing_type, :pickup_time, :state, :amount, :process, :payment_option
json.pickup_location shipment.pickup_full_address
json.drop_location shipment.drop_full_address
json.payment_option @shipment.try(:payment_option).try(:camelcase)
json.vehicle_type @shipment.try(:vehicle_type).try(:name)
json.detention @shipment.try(:bids).try(:best_bid).try(:detention)

json.vehicles do
	json.array! @shipment.vehicles do |vehicle|
	  json.id vehicle.id
	  json.registration_number vehicle.registration_number
		json.driver_name vehicle.try(:driver).try(:full_name)
	  json.driver_mobile vehicle.try(:driver).try(:mobile)
	  json.status vehicle.shipment_vehicles.where(:shipment_id => @shipment.id,vehicle_id: vehicle.id).first.status if vehicle.shipment_vehicles.where(:shipment_id => @shipment.id,vehicle_id: vehicle.id).first
	end
end
