class ShortListedBidder < ApplicationRecord
    belongs_to :fleet_owner, foreign_key: :fleet_owner_id, class_name: 'User'
    belongs_to :company
    belongs_to :shipment
end
