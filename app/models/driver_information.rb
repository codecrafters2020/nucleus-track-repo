class DriverInformation < ApplicationRecord
	audited

	acts_as_paranoid
	belongs_to :user
end
