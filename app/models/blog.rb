class Blog < ApplicationRecord
    audited

    has_one_attached :avatar
    
end
