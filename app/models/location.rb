class Location < ApplicationRecord
	audited
	belongs_to :country
	belongs_to :company

	validates :rate , presence: :true , numericality: {greater_than: 0 , message: I18n.t("greater_than_zero") }

	def contractual_locations
		"#{pickup_full_address} - #{drop_full_address} = #{try(:country).try(:currency)} #{rate.to_f}"
	end
	def pickup_full_address
		if pickup_location.include?(pickup_building_name.to_s)
			pickup_location
		else
			"#{pickup_building_name} #{pickup_location}"
		end
	end

	def drop_full_address
		if drop_location.include?(drop_building_name.to_s)
			drop_location
		else
			"#{drop_building_name} #{drop_location }"
		end
	end
end
