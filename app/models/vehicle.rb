class Vehicle < ApplicationRecord
	audited

	acts_as_mappable :default_units => :kms,
                   :default_formula => :sphere,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude
	belongs_to :company
	belongs_to :vehicle_type
	has_many :shipment_vehicles
	has_many :shipments, :through => :shipment_vehicles
	has_many :shipment_action_dates
	belongs_to :driver, foreign_key: :user_id, class_name: 'User', optional: true

	scope :_not_available_from, -> (from) {where('not_available_from >= ?', from)}
	scope :_not_available_to, -> (to) {where('not_available_to <= ?', to)}

	scope :not_availabile, -> (from,to) {where('not_available_from >= ? AND not_available_to <= ?', from,to)}
	
	scope :reterive_with_driver, -> {where.not(user_id: nil, user_id: 0)}

	scope :availabile_vehicles, -> {where(available: true)}
	scope :not_availabile_vehicles, -> {where(available: false)}
	scope :unavailable_vehicles, -> {where('not_available_to < ? AND available = ? ', Date.today, false)}
	
	scope :fleet_active, -> (company_id) {includes(:bids).where(bids: {company_id: company_id,status: [:open]}, state: "posted")}

	scope :if_not_assigned_to_shipment, -> {eager_load(:shipment_vehicles).where('shipment_vehicles.vehicle_id = ? OR shipment_vehicles.status = ? ',nil,6 )}

	scope :unoccupied_vehicles, -> {availabile_vehicles.reterive_with_driver}
	# scope :if_not_assigned_to_shipment, -> {includes(:shipments).where.not(shipments: { state: ["accepted","posted","ongoing", "vehicle_assigned"] })}

	scope :fetch_by_company, -> (company_id){where(:company_id => company_id)  }

	def vehicle_name
		registration_number.to_s + " - " + (driver.full_name rescue "")
	end

	class << self
		def update_vehicle_availability
			unavailable_vehicles.each do |vehicle|
				vehicle.available = true
				vehicle.save
			end
		end

		def reterive_for_assignment(current_user)
			unoccupied_vehicles.fetch_by_company(current_user.company_id)
		end

		def vehicles_with_shipment
			unoccupied_vehicles.joins(:shipment_vehicles,:shipments).group(:id)
		end

		def unavailable_vehicles_with_shipment
			not_availabile_vehicles.reterive_with_driver.joins(:shipment_vehicles,:shipments).group(:id)
		end

		def search_by_filters(current_user,params)
			if booked?(params) #booked are those whose shipment pickup date are within date ranges
			  @vehicles = vehicles_with_shipment.fetch_by_company(current_user.company_id)
			  @vehicles = filter_by_date(params,@vehicles)
			elsif available?(params) #available are those whose shipment pickup date  are not within date ranges
			  @vehicles = vehicles_with_shipment.fetch_by_company(current_user.company_id)
			  @vehicles = filter_by_available_date(params,@vehicles)
			elsif not_availabe?(params)
			   @vehicles = unavailable_vehicles_with_shipment.fetch_by_company(current_user.company_id)
			   @vehicles = filter_by_not_available_dates(@vehicles,params)
			end
			@vehicles
		end

		def filter_by_range_dates(params)
			filter_to_range_date(params).filter_by_from_range_date(params)
		end

		def filter_to_range_date(params)
			where("shipments.pickup_date  <= ?",params[:not_available_to])
		end

		def filter_by_from_range_date(params)
			where("shipments.pickup_date  >= ?",params[:not_available_from])
		end

		def filter_by_date(params,vehicles)
		  if dates_not_blank?(params)
				vehicles = vehicles.filter_by_range_dates(params) 
		  elsif date_to_not_blank?(params)
				vehicles = vehicles.filter_to_range_date(params)
		  elsif date_from_not_blank?(params)
				vehicles = vehicles.filter_by_from_range_date(params)
		  end  
		  vehicles
		end


		def filter_by_available_date(params,vehicles)
			  if dates_not_blank?(params)
					vehicles = vehicles.where("shipments.pickup_date  <= ? OR shipments.pickup_date  >= ?",params[:not_available_from],params[:not_available_to])
			  elsif date_to_not_blank?(params)
					vehicles = vehicles.where("shipments.pickup_date  >= ?",params[:not_available_to])
			  elsif date_from_not_blank?(params)
					vehicles = vehicles.where("shipments.pickup_date  <= ?",params[:not_available_from])
			  end  
			  vehicles
		end


		def filter_by_not_available_dates(vehicles,params)
			if dates_not_blank?(params)
			  vehicles = vehicles.not_availabile(params[:not_available_from],params[:not_available_to])
			elsif date_to_not_blank?(params)
			  vehicles = vehicles._not_available_to(params[:not_available_to])
			elsif date_from_not_blank?(params)
			  vehicles = vehicles._not_available_from(params[:not_available_from])
			end
			vehicles
		end

		def dates_not_blank?(params)
		  params[:not_available_from].present? && params[:not_available_to].present?
		end

		def date_to_not_blank?(params)
		  params[:not_available_to].present?
		end

		def date_from_not_blank?(params)
		  params[:not_available_from].present?
		end


		def booked?(params)
		  params[:state] == 'booked'
		end

		def available?(params)
		  params[:state] == 'available'      
		end

		def not_availabe?(params)
		  params[:state] == 'not-available'      
		end
	end


	end

