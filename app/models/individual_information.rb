class IndividualInformation < ApplicationRecord
  audited
  belongs_to :company
  after_create :blacklist_company
  # has_many_attached :attachments , dependent: :destroy


  # mount_base64_uploader :avatar, AvatarUploader
  # mount_base64_uploaders :documents, DocumentUploader
  def complete?
    national_id.present? and expiry_date.present?
  end

  def blacklist_company
    if self.company.company_type.individual? and self.company.category.cargo?
            self.company.update_column(:blacklisted, false)
    else
            self.company.update_column(:blacklisted, true)
    end
	end
end
