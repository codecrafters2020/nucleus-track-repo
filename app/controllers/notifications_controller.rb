class NotificationsController < ApplicationController
  def mark_viewed
    current_user.notifications.update_all(viewed_flag: true)
    respond_to do |f|
      f.js
    end
  end
end
