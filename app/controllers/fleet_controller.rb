class FleetController < ActionController::Base
  layout 'fleet'
  before_action :authenticate_user!
  before_action :check_if_fleet_owner
  include UserConcern

  def check_if_fleet_owner
    if !(current_user and current_user.fleet_owner?)
      flash[:error] =  'You are not authorized to access this page'
      redirect_to root_path
    elsif current_user and current_user.try(:company).try(:blacklisted?)
      signout_blacklisted
    elsif current_user and current_user.try(:company).try(:is_max_period_amount_overdue?) and !(["/fleet/payments","/fleet", "/fleet/shipments/completed"].include? request.path )
      flash[:error] =  I18n.t 'max_period_amount_overdue'
      redirect_to  fleet_root_path
    else
      proceed_if_profile_complete?
    end
  end

  def set_drivers_title
    @drivers_title = __method__
  end

  def check_if_individual
    redirect_back(fallback_location: fleet_root_path) if  current_user.company.individual?
  end

  def check_if_individual_and_vehicles_present
    redirect_back(fallback_location: fleet_root_path) if  current_user.company.individual? and  current_user.company.vehicles.present?
  end

  private

  	def proceed_if_profile_complete?
  		unless current_user.company and (current_user.company.company_information or current_user.company.individual_information)
  			flash[:error] = 'Please complete your profile to proceed further'
  			redirect_to edit_companies_path
  		end 
    end

  end