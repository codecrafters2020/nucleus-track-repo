class Cargo::ShipmentsController < CargoController

  include ShipmentConcern
  # include VehicleTypeConcern

  before_action :set_cargo_shipment, only: [:show,:cancel, :edit, :update, :destroy,:update_destination,:get_coordinates]
  before_action :set_vehicle_type, only: [:edit , :new, :create , :update]
  before_action :set_pickup_time, only: [:create, :update]

  # GET /cargo/shipments/1
  # GET /cargo/shipments/1.json
  def show
    redirect_to root_path, error: "Unauthorized" if @shipment.state != "posted" and @shipment.company.id !=  current_user.company_id
    @title = __method__.capitalize
    instance_variable_set :"@#{@shipment.state}" , "active"
  end

  # GET /cargo/shipments/new
  def new
    instance_variable_set :"@#{__method__}" , "active"
    @title = "Upcoming"
    @shipment = Shipment.new
    shipment = cookies[:shipment] ? eval(cookies[:shipment]) : nil
    if shipment
      shipment = eval(cookies[:shipment])
      @shipment = Shipment.new(pickup_location: shipment["pickup_location"],drop_location: shipment["drop_location"],
                              vehicle_type_id: shipment["vehicle_type_id"],no_of_vehicles: shipment["no_of_vehicles"],pickup_building_name: shipment["pickup_building_name"],drop_building_name: shipment["drop_building_name"], is_pickup_now: shipment["is_pickup_now"])
      cookies[:shipment] = nil
    end
  end

  # GET /cargo/shipments/1/edit
  def edit
    @title = __method__.capitalize
    if ["posted"].include?  @shipment.state

      # @shipment.pickup_date = @shipment.pickup_date.strftime("%m/%d/%y")
      # @shipment.expected_drop_off = @shipment.expected_drop_off.strftime("%m/%d/%y")
    else
      redirect_back(fallback_location: cargo_root_path)
    end
  end

  def calculate_fare
    @shipment = Shipment.new cargo_shipment_params
    fare = @shipment.prorate_formula
    currency = current_user.try(:country).try(:currency)
    render json: {fare: fare , currency: currency}, status: :ok
  end

  def posted
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id).posted
  end

  def ongoing
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id).ongoing
  end

  def rate_shipment
    if request.post?

    else
      respond_to do |format|
        format.html
        format.js
      end
    end
  end

  def upcoming
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id, state: ["accepted", "vehicle_assigned"])
  end
  def cancel
    respond_to do |format|
      format.js
    end
  end
  def completed
    @title = __method__.capitalize
    instance_variable_set :"@#{__method__}" , "active"
    @shipments = Shipment.where(company_id: current_user.company_id).where('state =? OR (state=? AND cancel_by=?)', "completed",'cancel','cargo_owner')
    # @shipments = Shipment.where(company_id: current_user.company_id, state: ["completed", "cancel"])
  end
  def update_destination
    if request.post? or request.patch?
      old_destination = @shipment.drop_location
      @change_destination_request = ChangeDestinationRequest.new(
          lat: params[:shipment][:drop_lat],
          lng: params[:shipment][:drop_lng],
          city: params[:shipment][:pickup_city],
          address: params[:shipment][:drop_location],
          drop_building_name: params[:shipment][:drop_building_name],
          shipment_id: @shipment.id )
      if @change_destination_request.save
        ShipmentMailer.change_destination_mail_to_admin(@shipment, old_destination, @change_destination_request.address)
        redirect_back(fallback_location: cargo_shipment_path(@shipment)) #, notice: "Shipment updated Successfully!"
        # render 'show'
      else
        puts @change_destination_request.errors.full_messages
        render json: { errors:  @shipment.errors.full_messages }, status: :bad_request
      end
    else
      respond_to do |format|
        format.html
        format.js
      end
    end
  end
  def get_coordinates
    # data = @shipment.shipment_vehicles.where(status: ShipmentVehicle::ONGOING_VEHICLES).map{|sv| {lat: (sv.vehicle.latitude.to_f rescue ""), lng: (sv.vehicle.longitude.to_f rescue ""), status: (sv.status.humanize rescue "") ,id: sv.id.to_s} }
    # render json: { coordinates:  data}, status: :ok
    render json: { coordinates:  get_shipment_coordinates(@shipment.id)}, status: :ok
  end

  def get_bulk_coordinates
    @shipments = current_user.shipments.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles:  {status: ShipmentVehicle::ONGOING_VEHICLES})
    render json: { coordinates:  get_shipment_bulk_coordinates(@shipments) }, status: :ok
  end


  # POST /cargo/shipments
  # POST /cargo/shipments.json
  def create
    # format_date_fields
    @shipment = Shipment.new(cargo_shipment_params)
    set_contract_fields if cargo_shipment_params[:location_id].present?
    @shipment.country_id = current_user.country_id
    respond_to do |format|
      if @shipment.save
        format.html { redirect_to posted_cargo_shipments_path, notice: 'Shipment was successfully created.' }
        format.json { render :show, status: :created, location: @shipment }
      else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /cargo/shipments/1
  # PATCH/PUT /cargo/shipments/1.json
  def update
    # format_date_fields
    respond_to do |format|
      if @shipment.update(cargo_shipment_params)
        @shipment.set_contract_fields
        @shipment.bids.destroy_all unless @shipment.state == "cancel"
        @shipment.shipment_posted_notifications
        format.html { redirect_to posted_cargo_shipments_path, notice: 'Shipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @shipment }
      else
        flash[:error] = @shipment.errors.full_messages
        format.html { render :edit }
      end
    end
  end

  # DELETE /cargo/shipments/1
  # DELETE /cargo/shipments/1.json
  def destroy
    @shipment.destroy
    respond_to do |format|
      format.html { redirect_to cargo_shipments_url, notice: 'Shipment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cargo_shipment
      shipment_country_access_control(params[:id])
      @shipment = Shipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def cargo_shipment_params
      params.require(:shipment).permit(:cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option, :pickup_building_name , :drop_building_name)
    end

    def format_date_fields
      params[:shipment][:pickup_date] = Date.strptime(params[:shipment][:pickup_date], "%m/%d/%Y")
      # params[:shipment][:expected_drop_off] = Date.strptime(params[:shipment][:expected_drop_off], "%m/%d/%Y") if params[:shipment][:expected_drop_off].present?
    end

    def set_pickup_time
      params[:shipment][:pickup_time] = set_time_zone(params[:shipment][:pickup_time]) if params[:shipment][:pickup_time].present?
    end

    def set_vehicle_type
      @vehicle_types = VehicleType.where(:country_id => current_user.try(:country_id)).order(:name)
    end

    def set_contract_fields
      location = Location.find(cargo_shipment_params[:location_id])
      @shipment.pickup_location = location.pickup_location
      @shipment.drop_location = location.drop_location
      @shipment.loading_time = location.loading_time
      @shipment.unloading_time= location.unloading_time
      @shipment.vehicle_type_id= location.vehicle_type_id
    end
end
