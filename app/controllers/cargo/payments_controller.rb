class Cargo::PaymentsController < CargoController
  def index
    @payment = :active
    @shipments = Shipment.where("(state = ? OR (state = ? AND cancel_by = ?)) AND company_id = ? AND paid_by_cargo = ? AND cancel_penalty_amount_received = ?","completed",'cancel','cargo_owner', current_user.company_id, false, false)
    # @shipments = Shipment.where(company_id: params[:company_id].to_i, paid_by_cargo: false)
  end
end
