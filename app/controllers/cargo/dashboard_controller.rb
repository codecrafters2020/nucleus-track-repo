class Cargo::DashboardController < CargoController
  before_action :authenticate_user!
  before_action :check_if_cargo_owner
  def index
    country = ISO3166::Country.find_country_by_name(current_user.country.name) rescue ""
    @user_country_coordinates = {lat: country.latitude , lng: country.longitude}
  	@shipments = current_user.shipments
  end
end
