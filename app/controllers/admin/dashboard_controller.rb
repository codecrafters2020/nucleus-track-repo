class Admin::DashboardController < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  before_action :set_company, only: [:mark_verified, :mark_featured, :mark_blacklisted, :mark_safe_for_cash_on_delivery, :tax_invoice]
  before_action :set_user, only: [:edit_user, :update_user,:show_user]
  before_action :set_cargo_owners_link, only: [:cargo_owners, :cargo_contracts]
  before_action :set_fleet_owners_link, only: [:fleet_owners]
  layout 'admin'

  def index
    country = ISO3166::Country.find_country_by_name(current_user.country.name) rescue ""
    @user_country_coordinates = {lat: country.latitude , lng: country.longitude}
    @shipments = Shipment.where(status: [:enroute , :onoing])
  end


  def cargo_owners
    @users = @cargo_owners = User.cargo_owners.reverse
    @filename = "Cargo Owners"
    respond_to_xls
  end

  def cargo_contracts
    @company = Company.find(params[:company_id])
    @locations = @company.locations
  end

  def fleet_owners
    @fleet_owners_link = "active"
    @users = @fleet_owners = User.fleet_owners.reverse
    @filename = "Fleet Owners"
    respond_to_xls
  end

  def mark_verified
    @company.update(verified: !@company.verified?)
  end

  def tax_invoice
    @company.update(tax_invoice: !@company.tax_invoice?) 
  end

  def respond_to_xls
    respond_to do |format|
     format.html
     format.xlsx {
           render xlsx: 'xlsx_users', filename: "#{@filename}.xlsx", disposition: 'attachment'
         }
      end  
  end
  
  def mark_featured
    @company.update(featured: !@company.featured?)
  end
  
  def mark_blacklisted
    @company.update(blacklisted: !@company.blacklisted?)
  end
  
  def mark_safe_for_cash_on_delivery
    @company.update(safe_for_cash_on_delivery: !@company.safe_for_cash_on_delivery?)
  end
  
  def show_user
  end
  
  def edit_user
    if @user.company.company_type == "company"
      @company_information =  @user.try(:company).try(:company_information)
      @company_information = @user.company.build_company_information unless @company_information.present?
    elsif @user.company.company_type == "individual"
      @individual_information =  @user.try(:company).try(:individual_information)
      @individual_information  = @user.company.build_individual_information unless @individual_information.present?
    end
  end
  
  def update_user
    @user.update(params[:user].permit(:first_name , :last_name , :mobile , :email, :country_id))
    set_credit_period
    if params[:user][:company][:company_information].present?
      if @user.try(:company).try(:company_information).present?
        @user.company.company_information.update(company_params)
      else
        @user.company.create_company_information(company_params)
      end
    elsif params[:user][:company][:individual_information].present?
      if @user.try(:company).try(:individual_information).present?
        @user.company.individual_information.update(individual_params)
      else
        @user.company.create_individual_information(individual_params)
      end
    end
    redirect_to admin_show_user_path(user_id: @user.id) , notice: "User updated successfully!"
  end
  

  private

  def set_company
    @company = Company.find(params[:company_id])
  end

  def set_credit_period
    credit_period = params[:user][:company_attributes][:credit_period] rescue nil
    credit_amount = params[:user][:company_attributes][:credit_amount] rescue nil
    is_contractual = params[:user][:company_attributes][:is_contractual] rescue nil
    tax_registration_no  = params[:user][:company_attributes][:tax_registration_no] rescue nil
    if @user.company.category == "cargo"
      @user.company.update(credit_amount: credit_amount ,credit_period: credit_period ,is_contractual: is_contractual)
    elsif @user.company.category == "fleet"
      @user.company.update(tax_registration_no: tax_registration_no)
    end
  end

  def set_user
    @user = User.find(params[:user_id])
    set_fleet_owners_link if @user.fleet_owner?
    set_cargo_owners_link if @user.cargo_owner?
  end

  def company_params
    params[:user][:company][:company_information].permit(:name , :address,:secondary_mobile_no, :landline_no,:website_address,:license_number, :license_expiry_date,:secondary_contact_name , :secondary_mobile_no, :news_update, :attachments,:avatar,{documents: []})
  end

  def individual_params
    params[:user][:company][:individual_information].permit(:secondary_mobile_no, :landline_no, :national_id, :expiry_date, :news_update)
  end

  def set_cargo_owners_link
    @cargo_owners_link = "active"
  end

  def set_fleet_owners_link
    @fleet_owners_link = "active"
  end

end
