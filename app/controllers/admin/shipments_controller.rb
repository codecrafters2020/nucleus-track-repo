class Admin::ShipmentsController < AdminController
  layout 'admin'
  before_action :set_shipment , only: [:show ,:update]
  before_action :set_shipment_link

  include ShipmentConcern

  def index
    params[:query] ||=   {}
    @shipments = Shipment.search(params[:query])

    respond_to do |format|
      format.html
     format.xlsx {
            start_date = params[:start_date].present? ? params[:start_date].to_date : 100.years.ago.to_date
            end_date = params[:end_date].present? ? params[:end_date].to_date : 100.years.from_now.to_date
            @shipments  = Shipment.where(created_at: start_date..end_date) #if params[:start_date].present? and params[:end_date].present?
            render xlsx: 'admin/index', filename: "all_shipments.xlsx", disposition: 'attachment'
         }
    end

  end

  def edit
    @shipment = Shipment.find params[:id]
    @vehicle_types = VehicleType.where(:country_id => @shipment.country_id)
  end

  def get_bulk_coordinates
    @shipments = Shipment.includes(:vehicle_type,:shipment_vehicles).where(shipment_vehicles: {status: ShipmentVehicle::ONGOING_VEHICLES })
    render json: { coordinates:  get_shipment_bulk_coordinates(@shipments) }, status: :ok
  end

  def cancel
    Shipment.find(params[:id]).update!(state_event: :cancel , cancel_by: current_user.role)
    redirect_to admin_shipments_path
    flash[:error]= "Shipment was canceled successfully"
  end
  

  def update   
    respond_to do |format|
      params[:shipment][:pickup_time] = set_time_zone(params[:shipment][:pickup_time]) if params[:shipment][:pickup_time]
      if @shipment.update!(shipment_params)
        @shipment.recalculate_shipment_payments if params[:shipment][:state_event] == "completed" || params[:shipment][:discount]
        @shipment.set_contract_fields
        format.html { redirect_to admin_shipments_path, notice: 'Shipment was successfully updated.' }
      else
        format.html { render :index }
      end
    end
  end

  def show
  end

  private

  def shipment_params
    params.require(:shipment).permit(:drop_building_name, :pickup_building_name, :completed_by, :discount, :cancel_reason, :cancel_by,:amount_per_vehicle ,:state_event, :state, :id,:company_id, :country_id, :pickup_location, :pickup_date, :pickup_time, :loading_time, :drop_location, :unloading_time, :expected_drop_off, :vehicle_type_id, :no_of_vehicles, :cargo_description, :cargo_packing_type, :pickup_lat, :pickup_lng, :drop_lat, :drop_lng, :drop_city, :pickup_city, :location_id, :amount, :payment_option)
  end

  def set_shipment
    @shipment = Shipment.find(params[:id])
  end

  def set_shipment_link
    @shipment_link = "active"
  end

end
