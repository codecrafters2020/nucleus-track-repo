class Admin::PagesController  < AdminController
  before_action :authenticate_user!
  before_action :check_if_admin
  
  layout 'admin'
  def index
    @pages=Page.all
  end

  def edit
    @page=Page.find(params[:id])
  end
  def update
    @page=Page.find(params[:id])
    @page.update(page_params)
    redirect_to admin_pages_path,notice:"Content updated successfully"
  end

  private
  def page_params
    params.require(:page).permit(:title,:body)
  end
end
