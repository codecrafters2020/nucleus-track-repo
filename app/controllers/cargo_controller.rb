class CargoController < ActionController::Base
	layout 'cargo'
  before_action :authenticate_user!
  before_action :check_if_cargo_owner
  include UserConcern

  def check_if_cargo_owner
    if !(current_user and current_user.cargo_owner?)
    	flash[:error] = I18n.t 'unauthorized_access'
    	redirect_to root_path
		elsif current_user and current_user.try(:company).blacklisted?
			signout_blacklisted
		elsif current_user and current_user.company.is_max_period_amount_overdue? and !(["/cargo/payments","/cargo", "/cargo/shipments/completed"].include? request.path )
			flash[:error] =  I18n.t 'max_period_amount_overdue'
			redirect_to  cargo_root_path
    else
    	proceed_if_profile_complete?
    end
  end


	private

	def proceed_if_profile_complete?
		unless current_user.company and (current_user.company.company_information or current_user.company.individual_information)
			flash[:error] = I18n.t 'incomplete_profile'
			if current_user.company.company_type == "company"
				redirect_to get_company_information_companies_path
			elsif current_user.company.company_type == "individual"
				redirect_to get_individual_information_companies_path
			end
		end
	end

end