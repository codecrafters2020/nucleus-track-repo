class InvoicesController < ApplicationController
  before_action :set_shipment , only: [:show]
  def show
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        @pdf_view = true
        render pdf: "Invoice",
           template: "/invoices/pdf.html.erb",
           :encoding => "utf8",
           layout: "pdf.html.erb"
      end
    end
  end
  private
  def set_shipment
    @shipment = Shipment.find(params[:id])
  end
end
