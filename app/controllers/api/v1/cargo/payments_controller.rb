class Api::V1::Cargo::PaymentsController < Api::V1::ApiController
	def index

		@shipments = Shipment.where("(state = ? OR (state = ? AND cancel_by = ?)) AND company_id = ? AND paid_by_cargo = ? AND cancel_penalty_amount_received = ?","completed",'cancel','cargo_owner', params[:company_id].to_i, false, false)
		# @shipments = Shipment.where(company_id: params[:company_id].to_i, paid_by_cargo: false)
	end
end
