class Api::V1::CountriesController < Api::V1::ApiController
  
  before_action :verify_jwt_token, except: [:index]
  before_action :set_country, only: [:show, :edit, :locations, :vehicles]
  def index
    @countries = Country.all
    render json: @countries, status: :ok
  end

  def locations
    @company = @user.company
  	@locations = @company.locations
  end

  def vehicles
	  #@vehicles = @country.vehicle_types.join(:avatar_attachment).order(:name)
    @vehicles = VehicleType.eager_load(:avatar_attachment).where(:country_id => @country.id).order(:name)
  end

  private
  def set_country
    @country = Country.find_by_id(params[:id])
  end
end
