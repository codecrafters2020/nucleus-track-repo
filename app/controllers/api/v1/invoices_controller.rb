class Api::V1::InvoicesController < Api::V1::ApiController
	before_action :verify_jwt_token

	def send_in_email
		# binding.pry
		InvoiceMailer.send_in_email(@user,params[:id]).deliver_now
		render json: {success: "Invoice successfully sent in email"}
	end

	
	private
	def set_shipment
	  @shipment = Shipment.find(params[:id])
	end
end
