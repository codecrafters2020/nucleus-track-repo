class PagesController < ApplicationController
  def show
    @page=Page.find_by(page_type:params[:id])
    render template: "pages/#{params[:id]}",locals:{page:@page}
  end
end
