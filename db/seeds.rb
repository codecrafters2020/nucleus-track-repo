# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.create(email: "waqar.hassan@virtualforce.io", password: "waqar123" ,role: "admin")
# Country.create( name: "pakistan", dialing_code: "+92", process: "bidding")

c = Country.create( name: "United Arab Emirates", dialing_code: "+971", process: "bidding", commision: 10.0, lorryz_share_tax: 5.0, currency: "AED", penalty_free_hours: 24.0, status: true, fleet_owner_income_tax: 5.0, fleet_max_days: 30, fleet_max_amount: 2000.0, fleet_penalty_free_hours: 0.0, short_name: "AE", cargo_max_amount: 10000.0, cargo_max_days: 30)

[
{code: 1, name: "Dyna Dry Pickup (Covered Top) – 2 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 2, name: "Dyna Dry Pickup (Open Top) – 5 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 3, name: "Dyna Pickup (Open Top) – 3 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 4, name: "Dyna Dry Pickup (Covered Top) – 5 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 5, name: "Dyna Dry Pickup (Open Top) – 10 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 6, name: "Dyna Dry Pickup (Covered Top) – 10 Tons", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 7, name: "Dyna Reefer Pickup – 5 Tons", country_id: c.id, fleet_penalty_amount: 0.75e2, cargo_penalty_amount: 0.1e3, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 8, name: "Dyna Reefer Pickup – 10 Tons", country_id: c.id, fleet_penalty_amount: 0.75e2, cargo_penalty_amount: 0.1e3, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 13, name: "20ft Container Truck & Trailer (22 Tons)", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 14, name: "40ft Container Truck & Trailer (26 Tons)", country_id: c.id, fleet_penalty_amount: 0.5e2, cargo_penalty_amount: 0.75e2, base_rate: 0.0, free_kms: 0.0, rate_per_km: 0.0, waiting_charges: 0.0, load_unload_free_hours: 0.0}, 
{code: 101, name: "20ft Container Truck & Trailer with Crane/Side Loader  (22 Tons)", country_id: c.id, fleet_penalty_amount: 0.2e3, cargo_penalty_amount: 0.4e3, base_rate: 0.4e3, free_kms: 5.0, rate_per_km: 0.8e2, waiting_charges: 0.8e2, load_unload_free_hours: 5.0}, 
{code: 102, name: "40ft Container Truck & Trailer with Crane/Side Loader  (26 Tons)", country_id: c.id, fleet_penalty_amount: 0.2e3, cargo_penalty_amount: 0.4e3, base_rate: 0.4e3, free_kms: 5.0, rate_per_km: 0.8e2, waiting_charges: 0.8e2, load_unload_free_hours: 5.0},
{code: 105, name: "Car Recovery Trucks (With Crane)", country_id: c.id, fleet_penalty_amount: 0.3e3, cargo_penalty_amount: 0.6e3, base_rate: 0.5e3, free_kms: 5.0, rate_per_km: 0.9e2, waiting_charges: 0.9e2, load_unload_free_hours: 7.0}, 
{code: 106, name: "Car Transporter Truck & Trailer (For 6 Sedan Car)", country_id: c.id, fleet_penalty_amount: 0.3e3, cargo_penalty_amount: 0.6e3, base_rate: 0.5e3, free_kms: 5.0, rate_per_km: 0.9e2, waiting_charges: 0.9e2, load_unload_free_hours: 7.0}, 
].each do |r|
	VehicleType.find_or_create_by(r)
end
