class AddCancelPenaltyAmountReceivedInShipments < ActiveRecord::Migration[5.2]
  def change
  	add_column :shipments,:cancel_penalty_amount_received, :boolean,default: false
  end
end
