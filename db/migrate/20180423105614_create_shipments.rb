class CreateShipments < ActiveRecord::Migration[5.2]
  def change
    create_table :shipments do |t|
      t.string :pickup_location
      t.string :pickup_lat
      t.string :pickup_lng
      t.string :drop_location
      t.string :drop_lat
      t.string :drop_lng
      t.date :pickup_date                   
      t.datetime :pickup_time               
      t.float :loading_time
      t.datetime :expected_drop_off
      t.float :unloading_time
      t.string :vehicle_type
      t.integer :no_of_vehicles
      t.text :cargo_description
      t.string :cargo_packing_type
      t.integer :company_id,                   index:true
      t.integer :vehicle_id,                   index:true
      t.boolean :approved,              			default: false
      t.integer :shipping_request_type_id,     index:true

      t.timestamps
    end
    
  end
end



