class CreateSessionLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :session_logs, id: :bigint do |t|
      t.integer :user_id
      t.integer :company_id, null: false
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
    add_column :users, :session_log_id, :bigint
  end
end
