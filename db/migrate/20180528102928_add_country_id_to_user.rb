class AddCountryIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :country_id, :integer
    remove_column :users, :country, :string
  end
end
