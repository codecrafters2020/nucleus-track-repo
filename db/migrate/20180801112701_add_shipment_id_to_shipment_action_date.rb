class AddShipmentIdToShipmentActionDate < ActiveRecord::Migration[5.2]
  def change
    add_column :shipment_action_dates, :shipment_id, :integer
    add_column :shipment_action_dates, :vehicle_id, :integer
  end
end
