class AddDefaultToCountryMaxDaysAndMaxAmount < ActiveRecord::Migration[5.2]
  def change
     change_column :countries, :cargo_max_amount, :float , default: 0.0
     change_column :countries, :cargo_max_days, :integer, default: 0
     change_column :countries, :fleet_max_amount , :float, default: 0.0
     change_column :countries, :fleet_max_days , :integer, default: 0
  end
end
