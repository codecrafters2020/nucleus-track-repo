class AddCancelReasonToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :cancel_reason, :text
  end
end
