class AddDocumentsToIndividualInformations < ActiveRecord::Migration[5.2]
  def change
    add_column :individual_informations, :documents, :string, array: true, default: []
  end
end
