class AddLatLngBids < ActiveRecord::Migration[5.2]
  def change
    add_column :bids, :lat, :float
    add_column :bids, :lng, :float
  end
end
