class AddDiscountToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :discount, :float, default: false
  end
end
