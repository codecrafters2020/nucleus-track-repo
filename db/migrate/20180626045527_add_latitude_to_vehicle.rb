class AddLatitudeToVehicle < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicles, :latitude, :string
    add_column :vehicles, :longitude, :string
  end
end
