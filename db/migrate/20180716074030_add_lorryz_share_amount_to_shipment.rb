class AddLorryzShareAmountToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :lorryz_share_amount, :decimal
    add_column :shipments, :fleet_income, :decimal
    add_column :shipments, :amount_tax, :decimal
    add_column :shipments, :detention, :decimal
    add_column :shipments, :lorryz_detention_share_amount, :decimal
    add_column :shipments, :fleet_detention_income, :decimal
    add_column :shipments, :detention_tax, :decimal
  end
end
