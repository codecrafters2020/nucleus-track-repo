class AddTaxToCountry < ActiveRecord::Migration[5.2]
  def change
    add_column :countries, :tax, :float
    add_column :countries, :currency, :string
    add_column :countries, :penalty_free_hours, :float
    add_column :countries, :status, :boolean
  end
end
