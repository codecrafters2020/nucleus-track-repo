class AddDocumentFieldsToVehicles < ActiveRecord::Migration[5.2]
  def change
  	add_column :vehicles, :documents, :string, array: true, default: []
  end
end
