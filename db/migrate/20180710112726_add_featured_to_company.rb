class AddFeaturedToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :featured, :boolean, default: false
  end
end
