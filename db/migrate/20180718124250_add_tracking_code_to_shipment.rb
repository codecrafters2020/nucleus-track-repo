class AddTrackingCodeToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :tracking_code, :string
  end
end
