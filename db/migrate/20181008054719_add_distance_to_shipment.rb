class AddDistanceToShipment < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :distance_between_endpoints, :float
  end
end
