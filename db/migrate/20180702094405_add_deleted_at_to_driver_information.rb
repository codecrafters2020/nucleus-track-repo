class AddDeletedAtToDriverInformation < ActiveRecord::Migration[5.2]
  def change
    add_column :driver_informations, :deleted_at, :datetime
    add_index :driver_informations, :deleted_at
  end
end
