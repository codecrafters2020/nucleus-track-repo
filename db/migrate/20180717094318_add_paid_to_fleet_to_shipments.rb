class AddPaidToFleetToShipments < ActiveRecord::Migration[5.2]
  def change
    add_column :shipments, :paid_to_fleet, :boolean, default: false
    add_column :shipments, :paid_by_cargo, :boolean, default: false
  end
end
