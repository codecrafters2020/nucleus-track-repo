class AddOnlineUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_online, :boolean, default: true
  end
end
