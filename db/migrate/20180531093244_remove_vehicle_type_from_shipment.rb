class RemoveVehicleTypeFromShipment < ActiveRecord::Migration[5.2]
  def change
  	add_column :shipments, :vehicle_type_id, :integer
    remove_column :shipments, :vehicle_type, :string
  end
end
