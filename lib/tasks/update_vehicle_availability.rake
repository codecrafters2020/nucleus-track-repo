desc 'Update Status if vehicle available'
task update_vehicle_availability: :environment do

  puts "Update Vehicle availablility"
  Vehicle.update_vehicle_availability

end