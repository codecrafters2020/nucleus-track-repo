desc 'Notification to Fleet for the reminder of Assign Vehicle'
task assign_vehicle_reminder_notification: :environment do

  puts "Shipment reminder notification"

  shipments = Shipment.accepted

  shipments.each do |shipment|
    hours = ((shipment.pickup_time - Time.zone.now )/3600).round
    if hours >=20 and hours < 24
      shipment.notify_fleet_to_assign_vehicle
    end
  end

end