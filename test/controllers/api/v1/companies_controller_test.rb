require 'test_helper'

class Api::V1::CompaniesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @api_v1_company = api_v1_companies(:one)
  end

  test "should get index" do
    get api_v1_companies_url
    assert_response :success
  end

  test "should get new" do
    get new_api_v1_company_url
    assert_response :success
  end

  test "should create api_v1_company" do
    assert_difference('Api::V1::Company.count') do
      post api_v1_companies_url, params: { api_v1_company: { category: @api_v1_company.category, company_type: @api_v1_company.company_type, user_id: @api_v1_company.user_id } }
    end

    assert_redirected_to api_v1_company_url(Api::V1::Company.last)
  end

  test "should show api_v1_company" do
    get api_v1_company_url(@api_v1_company)
    assert_response :success
  end

  test "should get edit" do
    get edit_api_v1_company_url(@api_v1_company)
    assert_response :success
  end

  test "should update api_v1_company" do
    patch api_v1_company_url(@api_v1_company), params: { api_v1_company: { category: @api_v1_company.category, company_type: @api_v1_company.company_type, user_id: @api_v1_company.user_id } }
    assert_redirected_to api_v1_company_url(@api_v1_company)
  end

  test "should destroy api_v1_company" do
    assert_difference('Api::V1::Company.count', -1) do
      delete api_v1_company_url(@api_v1_company)
    end

    assert_redirected_to api_v1_companies_url
  end
end
