require 'test_helper'

class VechilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vechile = vechiles(:one)
  end

  test "should get index" do
    get vechiles_url
    assert_response :success
  end

  test "should get new" do
    get new_vechile_url
    assert_response :success
  end

  test "should create vechile" do
    assert_difference('Vechile.count') do
      post vechiles_url, params: { vechile: { company_id: @vechile.company_id, insurance_number: @vechile.insurance_number, load_capacity: @vechile.load_capacity, registration_number: @vechile.registration_number, vechile_type: @vechile.vechile_type } }
    end

    assert_redirected_to vechile_url(Vechile.last)
  end

  test "should show vechile" do
    get vechile_url(@vechile)
    assert_response :success
  end

  test "should get edit" do
    get edit_vechile_url(@vechile)
    assert_response :success
  end

  test "should update vechile" do
    patch vechile_url(@vechile), params: { vechile: { company_id: @vechile.company_id, insurance_number: @vechile.insurance_number, load_capacity: @vechile.load_capacity, registration_number: @vechile.registration_number, vechile_type: @vechile.vechile_type } }
    assert_redirected_to vechile_url(@vechile)
  end

  test "should destroy vechile" do
    assert_difference('Vechile.count', -1) do
      delete vechile_url(@vechile)
    end

    assert_redirected_to vechiles_url
  end
end
